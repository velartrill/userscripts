#!/bin/bash
# [ʞ] xbl.sh - x backlight
# set screen brightness with dmenu

BL=/sys/class/backlight/nv_backlight

#$BL/brightness must be user-writable!
#zenity --scale --title "brightness" --text "set monitor brightness" --value=$(cat $BL/brightness) --max-value=$(cat $BL/max_brightness) > $BL/brightness

brightness=$(echo -n | dmenu -fn "Open Sans 15" -nb "#6A323F" -nf "#FFC1CF" -sb "#220008" -sf  "#fff" -b -p "brightness [$(cat $BL/brightness)]")
maxbr=$(cat $BL/max_brightness)
if [ $brightness -le $maxbr ]; then
	echo $brightness > $BL/brightness
fi
