#!/bin/bash
# [ʞ] lnc.sh - launch
# pick a file with dmenu and launch an app to edit it
if test $# -eq 3; then
	cd $3
fi
while :; do
	# hideous one-liner to exclude binaries
	file=$(for i in *; do if test -d $i; then echo $i; elif file --mime -b $i | grep "text/" >/dev/null; then echo $i; fi; done | dmenu -fn "Open Sans 15" -nb "#6A323F" -nf "#FFC1CF" -sb "#220008" -sf  "#fff" -b -p "$2")
	if test $? -eq 0; then
		if test -d $file; then
			cd $file
		else
			exec $1 $file
		fi
	else
		exit
	fi
done
