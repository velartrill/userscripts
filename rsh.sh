#!/bin/bash
# [ʞ] rsh.sh
# set screen temperature from dmenu

BL=/sys/class/backlight/nv_backlight

brightness=$(echo -n | dmenu -fn "Open Sans 15" -nb "#6A323F" -nf "#FFC1CF" -sb "#220008" -sf  "#fff" -b -p  'temperature [deactivate]')

result=$?
if [ "$result" -eq 0 ]; then
	maxbr=5000
	if [ $brightness -le $maxbr ]; then
		redshift -O $brightness
	else
		redshift -x
	fi
fi
