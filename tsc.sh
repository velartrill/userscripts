#!/bin/bash
# [ʞ] tsc.sh - twitter screenshot
# upload screencap directly to the awful bird website

cmd="/usr/bin/ruby /home/lexi/.gem/ruby/2.4.0/bin/t update"
img="/home/lexi/tmp/$(date +%s%N).png"
runfile="/home/lexi/.config/run/tsc.run"
if test $1 = "f"; then
	maim -o $img
else
	maim -so $img
fi
if test $? -eq 1; then
	exit
fi
caption=$(zenity --entry --title="Upload screenshot" --text="Key in caption for screenshot" --width=512)
if test $? -eq 0; then
	if test -e "$runfile"; then
		echo $(expr $(cat "$runfile") + 1) > "$runfile"
	else
		echo 0 > "$runfile"
	fi
	t update "$caption" -f $img

	#race condition here? idk man
	if test $(cat "$runfile") -eq 0; then
		rm "$runfile"
	else
		echo $(expr $(cat "$runfile") - 1) > "$runfile"
	fi
	rm $img
fi

