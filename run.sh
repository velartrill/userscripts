#!/bin/bash
# [ʞ] run.sh - run
# a dmenu wrapper that attempts to determine whether a program
# should be launched under a terminal

user=$(whoami)
host=$(hostname)
cmd=$(dmenu_path | dmenu -fn "Open Sans 15" -nb "#6A323F" -nf "#FFC1CF" -sb "#220008" -sf  "#fff" -b -p "$user@$host")
if test $? -eq 0; then
	echo launching $cmd
	target=$(cut -d\  -f1 <<< "$cmd")
	echo base command appears to be $target
	target=$(whereis $target | cut -d\  -f2)
	echo command location $target
	if ldd $target; then
		echo "file is binary, scanning deps"
		if ldd $target | grep "libX" >/dev/null; then
			echo "X11 found, binary appears to be visual"
			$cmd
		else
			echo "no X11, launching terminal"
			mate-terminal -x "$cmd"
		fi
	else
		echo result $?
		echo "either shell script or doesn't exist"
		echo "attempting plain launch"
		$cmd
	fi
else
	echo "dmenu canceled, giving up"
fi
