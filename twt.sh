#!/bin/bash
# [ʞ] twt.sh - tweet thread
# tweetrant gui using zenity

pidfile="/home/lexi/.config/run/twt.run"
user=velartrill

tweeted=0

waitfor() {
	while ps $1 >/dev/null; do
		sleep 1
	done
}

if test $1 = "n"; then	# new thread
	tweet=$(zenity --entry --title "Begin tweet thread" --width=500 --text="Key in initial tweet.")
	status=$?
	if test $status -eq 0; then
		t update "$tweet" &
		tweeted=1
	else
		exit
	fi
fi

reply() {
	echo "tweeting $1; last process $!; tweet status $tweeted"
	if test $tweeted -eq 1; then waitfor $!; fi
	local lastweet=$(t tl "$user" -l -n 1 | cut -d' ' -f1)
	echo "replying to $lastweet"
	t reply "$lastweet" "$tweet"
}

if test -e $pidfile; then
	zenity --error --text="PID file exists; aborting to prevent race condition."
else
	echo "$$" > $pidfile
	status=0
	while test $status -eq 0; do
		tweet=$(zenity --entry --title "Thread tweets" --width=500 --text="Key in tweet.")
		status=$?
		if test $status -eq 0; then
			reply "$tweet" &
			tweeted=1
		fi
	done
	wait
	rm "$pidfile"	
fi

